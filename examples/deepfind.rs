extern crate rigi;
extern crate walkdir;

use std::str;
use walkdir::WalkDir;
use std::path::Path;
use std::env;
use rigi::streams::FileInputStream;
use rigi::streams::Stream;
use rigi::streams::StreamInfo;
use rigi::streams::TarStream;

fn handle_stream(stream: &mut Stream<u8>) {
    if let Ok(mut tar) = TarStream::new(stream) {
        loop {
            match tar.next().unwrap() {
                None => {
                    break
                },
                Some(mut s) => handle_entry(&mut s)
            };
        }
    }
}

fn handle_entry(stream: &mut StreamInfo<u8>) {
    println!("{} {:?}", str::from_utf8(stream.file_name().unwrap()).unwrap(),
            stream.file_type());
    if let Some(mut s) = stream.stream() {
        handle_stream(s);
    }
}

fn deepfind(file_path: &Path) {
    let mut fi = FileInputStream::open(file_path).unwrap();
    handle_stream(&mut fi);
}

fn main() {
    let path = env::args().nth(1).unwrap();
    for entry in WalkDir::new(path) {
        let entry = entry.unwrap();
        println!("{}", entry.path().display());
        if entry.file_type().is_file() {
            deepfind(entry.path());
        }
    }
}
