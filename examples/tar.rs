extern crate rigi;

use rigi::streams::FileInputStream;
use rigi::streams::Stream;
use rigi::streams::StreamInfo;
use rigi::streams::TarStream;
use std::env;
use std::str;

fn handle_stream(stream: &mut StreamInfo<u8>) {
    println!("{} {:?}", str::from_utf8(stream.file_name().unwrap()).unwrap(),
            stream.file_type());
    if let Some(mut s) = stream.stream() {
        loop {
            match s.read_min(0) {
                Err(e) => {
                    println!("{:?}", e);
                    return;
                },
                Ok(buf) => {
                    if buf.len() == 0 {
                        break;
                    }
                    //stdout().write(&buf);
                    ()
                }
            }
        }
    }
}

fn main() {
    let path = env::args().nth(1).unwrap();
    let mut fi = FileInputStream::open(path).unwrap();
    let mut tar = TarStream::new(&mut fi).unwrap();
    loop {
        match tar.next().unwrap() {
            None => {
                break
            },
            Some(mut s) => handle_stream(&mut s)
        };
    }
}
