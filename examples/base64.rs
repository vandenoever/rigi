extern crate rigi;

use rigi::streams::Base64InputStream;
use rigi::streams::FileInputStream;
use rigi::streams::Stream;
use std::env;
use std::io::{stdout,Write};

fn main() {
    let path = env::args().nth(1).unwrap();
    let mut fi = FileInputStream::open(path).unwrap();
    let mut base64 = Base64InputStream::base64(&mut fi);
    loop {
        match base64.read_min(0) { // todo: use char stream
            Ok(buffer) if buffer.len() == 0 => {
                break;
            },
            Ok(buffer) => {
                stdout().write(&buffer).unwrap();
            },
            Err(err) => {
                println!("{:?}", err);
                panic!(err.to_string());
            }
        }
    }
}
