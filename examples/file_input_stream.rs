extern crate rigi;

use rigi::streams::Base64InputStream;
use rigi::streams::FileInputStream;
use rigi::streams::Stream;
use std::str;

fn main() {
    let path = "/home/oever/src/strigi/libstreams/tests/data/base64enc.txt";
    let mut fi = match FileInputStream::open(path) {
        Ok(fi) => fi,
        Err(_) => return
    };
    let mut base64 = Base64InputStream::base64(&mut fi);
    loop {
        match base64.read_min(0) {
            Ok(buffer) if buffer.len() == 0 => {
                break;
            },
            Ok(buffer) => {
                println!("{:?}", str::from_utf8(&buffer));
            },
            Err(err) => {
                println!("Err {:?}", err);
                break;
            }
        }
    }
    ()
}
