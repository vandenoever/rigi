use std::io;
use streams::Stream;
use streams::stream::read_beyond_eos;

pub struct Substream<'a,T: 'a> {
    stream: &'a mut Stream<T>,
    offset: u64,
    size: u64
}

impl<'a,T> Substream<'a,T> {
    pub fn new(stream: &mut Stream<T>, size: u64) -> io::Result<Substream<T>> {
        let offset = stream.position();
        // check that the desired size is not too long
        if let Some(parent_size) = stream.size() {
            if offset + size > parent_size {
                return Err(io::Error::new(io::ErrorKind::UnexpectedEof,
                    "Substream extends beyond the parent stream."));
            }
        }
        Ok(Substream{stream: stream, offset: offset, size: size})
    }
}

impl<'a,T> Stream<T> for Substream<'a,T> {
    fn position(&self) -> u64 {
        self.stream.position() - self.offset
    }
    fn size(&self) -> Option<u64> {
        Some(self.size)
    }
    fn read_min_max(&mut self, min: usize, max: usize) -> io::Result<&[T]> {
        assert!(self.size >= self.position());
        let left = self.size - self.position();
        // check if min extends beyond size
        if min as u64 > left {
            return Err(read_beyond_eos());
        }
        let max = if max as u64 > left { left as usize } else { max };
        self.stream.read_min_max(min, max)
    }
    fn rewind(&mut self, n: usize) -> () {
        if n as u64 > self.position() {
            panic!("Trying to rewind too much which leaks from parent stream.");
        }
        self.stream.rewind(n)
    }
}
