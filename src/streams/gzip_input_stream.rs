use flate2::Decompress;
use flate2::Flush;
use std::error::Error;
use std::io;
use streams::BufferInput;
use streams::BufferedStream;
use streams::Stream;

pub struct Gzip<'a> {
    stream: &'a mut Stream<u8>,
    decompress: Decompress
}

impl<'a> BufferInput for Gzip<'a> {
    type Element = u8;
    fn write_to_buffer(&mut self, buf: &mut [Self::Element]) -> io::Result<usize> {
        let in_buf = try!(self.stream.read_min(0));
        let total_out_before = self.decompress.total_out();
        println!("OK D");
        match self.decompress.decompress(in_buf, buf, Flush::None) {
            Err(err) => {
                return Err(io::Error::new(io::ErrorKind::InvalidInput,
                    err.description()));
            },
            Ok(_) => ()
        };
        let total_out_after = self.decompress.total_out();
        Ok((total_out_after - total_out_before) as usize)
    }
}

pub type GzipInputStream<'a> = BufferedStream<u8,Gzip<'a>>;

impl<'a> GzipInputStream<'a> {
    pub fn gzip(stream: &'a mut Stream<u8>, zlib_header: bool) -> GzipInputStream<'a> {
        let decompress = Decompress::new(zlib_header);
        BufferedStream::new(Gzip{
            stream: stream,
            decompress: decompress
        })
    }
}
