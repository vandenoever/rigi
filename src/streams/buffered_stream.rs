use std::cmp::min;
use std::convert::From;
use std::io;
use std::io::Read;
use streams::Stream;
use streams::StreamBuffer;

pub trait BufferInput {
    type Element;
    fn write_to_buffer(&mut self, buf: &mut [Self::Element]) -> io::Result<usize>;
}

pub struct BufferedStream<T: Clone + Copy + From<u8>, I: BufferInput<Element = T>> {
    size: Option<u64>,
    position: u64,
    buffer: StreamBuffer<T>,
    read: I,
    finished_writing_to_buffer: bool
}

impl<T: Clone + Copy + From<u8>,I: BufferInput<Element = T>> BufferedStream<T,I> {
    pub fn new(read: I) -> BufferedStream<T,I> {
        BufferedStream {
            size: None,
            position: 0,
            buffer: StreamBuffer::with_capacity(16380),
            read: read,
            finished_writing_to_buffer: false
        }
    }
    fn write_to_buffer(&mut self, min: usize) -> io::Result<()> {
        let mut missing = min;
        let mut nwritten = 0;
        {
            let mut buf = self.buffer.get_write_buffer(min);
            // read at least once from the input
            loop {
                match try!(self.read.write_to_buffer(buf)) {
                    0 => { // eof
                        self.finished_writing_to_buffer = true;
                        break;
                    },
                    n if n >= missing => {
                        nwritten += n;
                        missing = 0;
                        break;
                    }
                    n => {
                        nwritten += n;
                        missing -= n;
                    }
                }
            }
        }
        self.buffer.report_written(nwritten);
        if missing > 0 {
            if self.size.is_none() {
                self.size = Some(self.position + self.buffer.avail() as u64);
            }
            return Err(io::Error::new(io::ErrorKind::UnexpectedEof, ""));
        }
        Ok(())
    }
}

//impl<R: Seek> Seek for BufferedStream<R> {
//    fn seek(&mut self, pos: SeekFrom) -> io::Result<u64> {
//        let r = self.read.seek(pos);
//        if let Ok(p) = r {
//            self.buffer.reset();
//            self.position = p;
//        }
//        r
//    }
//}

impl<T: Clone + Copy + From<u8>,I: BufferInput<Element = T>> Stream<T> for BufferedStream<T,I> {
    fn position(&self) -> u64 {
        self.position
    }
    fn size(&self) -> Option<u64> {
        self.size
    }
    fn read_min_max(&mut self, min: usize, max: usize) -> io::Result<&[T]> {
        let max = if min > max { min } else { max };
        let avail = self.buffer.avail();
        if min > avail && self.finished_writing_to_buffer {
            // error: premature end of stream
            return Err(io::Error::new(io::ErrorKind::UnexpectedEof, ""));
        }
        // if min == 0 and buffer is empty, try to fill it
        if (min == 0 && avail == 0) || min > avail {
            try!(self.write_to_buffer(min - avail));
        }
        let (buf, avail) = self.buffer.read(max);
        self.position += buf.len() as u64;
        if self.size.is_some() && self.position > self.size.unwrap() {
            // error: we read more than was specified in size
            // this is an error because all dependent code might have been
            // labouring under a misapprehension
            return Err(io::Error::new(io::ErrorKind::InvalidData,
                "Could read more data than file size."));
        } else if avail == 0 && self.finished_writing_to_buffer {
            self.size = Some(self.position);
        }
        Ok(buf)
    }
    fn rewind(&mut self, n: usize) -> () {
        self.buffer.rewind(n);
        self.position -= n as u64;
    }
}
