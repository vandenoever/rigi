use std::cmp;
use std::io;

/// A trait for reading elements from a stream.
///
/// A struct can implement `Stream<T>` and `std::io::Read` and
/// `std::io::BufRead`.
pub trait Stream<T> {
    /// Return the current position in the stream.
    fn position(&self) -> u64;

    /// Return the size of the stream or `None` if the size is not known.
    fn size(&self) -> Option<u64>;

    /// Read at least `min` and at most `max` items. Returns a slice with
    /// the data or Err if there was a problem.
    /// If it is not possible to read `min` items, an `Err` is returned.
    fn read_min_max(&mut self, min: usize, max: usize) -> io::Result<&[T]>;

    /// Rewind a number of places.
    /// If one rewinds more than was read or skipped, the behavior is
    /// unspecified.
    /// This can be used to obtain a larger read than was previously requested.
    ///
    /// ```
    /// {
    ///     let buf = try!(stream.read_min(10);
    ///     ...
    /// }
    /// stream.rewind(10);
    /// let buf = try!(stream.read_min(20);
    /// ```
    fn rewind(&mut self, n: usize) -> ();

    /// Convenience function for reading exactly `n` items.
    /// This calls `read_min_max(n,n)`.
    fn read_n(&mut self, n: usize) -> io::Result<&[T]> {
        self.read_min_max(n, n)
    }
    /// Convenience function for reading at least `min` items.
    /// This calls `read_min_max(min, usize::max_value())`.
    /// If less than `min` items can be read, `Err` is returned and the stream
    /// stays at the previous position.
    fn read_min(&mut self, min: usize) -> io::Result<&[T]> {
        self.read_min_max(min, usize::max_value())
    }
    /// Skip exactly `n` items.
    /// If this fails, an `Err` is returned but the stream may have advanced.
    fn skip(&mut self, ntoskip: u64) -> io::Result<()> {
        // skip by reading
        let mut left = ntoskip;
        while left > 0 {
            let step = cmp::min(usize::max_value() as u64, left) as usize;
            let b = try!(self.read_min_max(1, step));
            left -= b.len() as u64;
        }
        Ok(())
    }
}

pub fn read_beyond_eos() -> io::Error {
    io::Error::new(io::ErrorKind::InvalidInput,
        "Trying to read beyond the end of the stream.")
}
