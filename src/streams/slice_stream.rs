use std::cmp;
use std::io;
use streams::Stream;

pub struct SliceStream<T> {
    pos: usize,
    slice: Vec<T>
}

impl<T> SliceStream<T> {
    pub fn new(slice: Vec<T>) -> SliceStream<T> {
        SliceStream{pos: 0, slice: slice}
    }
}

impl<T> Stream<T> for SliceStream<T> {
    fn position(&self) -> u64 {
        self.pos as u64
    }
    fn size(&self) -> Option<u64> {
        Some(self.slice.len() as u64)
    }
    fn read_min_max(&mut self, min: usize, max: usize) -> io::Result<&[T]> {
        let avail = self.slice.len() - self.pos;
        if min > avail {
            return Err(io::Error::new(io::ErrorKind::UnexpectedEof, ""));
        }
        let n = cmp::min(avail, max);
        let r = &self.slice[self.pos..self.pos + n];
        self.pos += n;
        Ok(r)
    }
    fn rewind(&mut self, n: usize) -> () {
        self.pos -= n;
    }
}
