use streams::Stream;

fn check_header(h: &[u8]) -> bool {
    let magic = [0x50, 0x4b, 0x03, 0x04];
    h.starts_with(&magic)
}

pub struct ZipInputStream<'a> {
    stream: &'a mut Stream<u8>,
}

impl<'a> ZipInputStream<'a> {
}
