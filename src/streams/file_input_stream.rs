use std::fs::File;
use std::io::Read;
use std::io;
use std::path::Path;
use streams::SeekableStream;
use streams::BufferInput;

impl BufferInput for File {
    type Element = u8;
    fn write_to_buffer(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.read(buf)
    }
}

pub type FileInputStream = SeekableStream<u8,File>;

impl FileInputStream {
    pub fn open<P: AsRef<Path>>(path: P) -> io::Result<FileInputStream> {
        let file = try!(File::open(path));
        FileInputStream::new(file)
    }
}
