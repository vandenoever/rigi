use streams::Stream;
use std::collections::hash_map::HashMap;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum FileType {
    Directory, File, Symlink, Unknown
}

pub trait StreamInfo<T> {
    fn file_name(&self) -> Option<&[u8]> {
        None
    }
    fn properties(&self) -> &HashMap<Vec<u8>,Vec<u8>>;
    fn mtime(&self) -> Option<i64> {
        None
    }
    fn file_type(&self) -> FileType {
        FileType::File
    }
    fn stream(&mut self) -> Option<&mut Stream<T>>;
}
