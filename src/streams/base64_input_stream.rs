use std::io;
use streams::BufferedStream;
use streams::BufferInput;
use streams::Stream;

pub struct Decoder<'a> {
    stream: &'a mut Stream<u8>,
    bytes_todo: u8,
    bits: u32
}

const T : bool = true;
const F : bool = false;
// all base64 characters in increasing value: A=0, B=1, etc
// ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/
// '=' is also counted as a base64 character
const IS_BASE64 : [bool; 256] = [
  F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,
  F,F,F,F,F,F,F,F,F,F,F,T,F,F,F,T,T,T,T,T,T,T,T,T,T,T,F,F,F,T,F,F,
  F,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,F,F,F,F,F,
  F,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,F,F,F,F,F,
  F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,
  F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,
  F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,
  F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F,F];

const TRANSLATION : [u8; 80] = [ // offset is 43 ('+')
  62, 0, 0, 0,63,52,53,54,55,56,57,58,59,60,61, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2,
   3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25, 0, 0,
   0, 0, 0, 0,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,
  47,48,49,50,51];

// only call this function when IS_BASE64[c] == true
fn translate(c: u8) -> u32 {
    TRANSLATION[(c - 43) as usize] as u32
}

// get iter, proceed until first char or end of list

fn skip_whitespace(buf: &[u8], pos: usize) -> usize {
    let mut pos = pos;
    while pos < buf.len() {
        let c = buf[pos];
        if c != 9 && c != 10 && c != 13 && c != 20 {
            return pos;
        }
        pos += 1;
    }
    pos
}

fn get_byte(buf: &[u8], pos: usize) -> io::Result<(Option<u8>,usize)> {
    if pos == buf.len() {
        return Ok((None, pos));
    }
    let c = buf[pos];
    if !IS_BASE64[c as usize] {
        return Err(io::Error::new(io::ErrorKind::InvalidData, ""));
    }
    Ok((Some(c), pos + 1))
}

// iterate to read a quad, possible outcomes
// - read a full quad: return quad and new position and # of found bytes
// - incomplete quad: return 0 and new position and # of found bytes (0)
// - bad data: return an error
fn parse_quad(buf: &[u8], pos: usize) -> io::Result<(u32,usize,u8)> {
    let (a, pos) = try!(get_byte(buf, pos));
    let pos = skip_whitespace(buf, pos);
    let (b, pos) = try!(get_byte(buf, pos));
    let pos = skip_whitespace(buf, pos);
    let (c, pos) = try!(get_byte(buf, pos));
    let pos = skip_whitespace(buf, pos);
    let (d, pos) = try!(get_byte(buf, pos));
    match (a, b, c, d) {
        (Some(a), Some(b), Some(c), Some(d)) => {
            // check for '='
            let bytes = if c == 60 { 1 } else if d == 60 { 2 } else { 3 };
            Ok(((translate(a) << 18)
              + (translate(b) << 12)
              + (translate(c) << 6)
              +  translate(d), pos, bytes))
        },
        _ => Ok((0, pos, 0))
    }
}

impl<'a> BufferInput for Decoder<'a> {
    type Element = u8;
    fn write_to_buffer(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        assert!(self.bytes_todo < 3);
        let outlen = buf.len();
        let mut outpos = 0;
        if outpos < outlen && self.bytes_todo == 2 {
            buf[outpos] = ((self.bits >> 8) & 0xff) as u8;
            outpos += 1;
            self.bytes_todo = 1;
        }
        if outpos < outlen && self.bytes_todo == 1 {
            buf[outpos] = (self.bits & 0xff) as u8;
            outpos += 1;
            self.bytes_todo = 0;
        }
        let mut inpos = 0;
        let mut inlen = 0;
        while outpos < outlen {
            let min_read;
            if inpos != inlen {
                // stream was not parsed to the end
                // rewind a bit before doing a new read
                self.stream.rewind(inlen - inpos);
                min_read = inlen - inpos + 1;
            } else {
                min_read = 0;
            }
            let inbuf = try!(self.stream.read_min(min_read));
            inpos = skip_whitespace(inbuf, 0);
            inlen = inbuf.len();
            if inpos == inlen {
                break;
            }
            while outpos < outlen {
                inpos = skip_whitespace(inbuf, inpos);
                let (bits,pos,n) = try!(parse_quad(inbuf, inpos));
                if n == 0 { // end of data
                    // incomplete quad: read more data next time
                    break;
                }
                inpos = pos;
                self.bits = bits;
                buf[outpos] = (self.bits >> 16) as u8;
                outpos += 1;
                if outpos >= outlen {
                    self.bytes_todo = 2;
                    break;
                }
                if n == 1 {
                    return Ok(outpos);
                }
                buf[outpos] = ((self.bits >> 8) & 0xff) as u8;
                outpos += 1;
                if outpos >= outlen {
                    self.bytes_todo = 1;
                    break;
                }
                if n == 2 {
                    return Ok(outpos);
                }
                buf[outpos] = (self.bits & 0xff) as u8;
                outpos += 1;
            }
        }
        Ok(outpos)
    }
}

pub type Base64InputStream<'a> = BufferedStream<u8,Decoder<'a>>;

impl<'a> Base64InputStream<'a> {
    pub fn base64(stream: &'a mut Stream<u8>) -> Base64InputStream<'a> {
        BufferedStream::new(Decoder{
            stream: stream,
            bits: 0,
            bytes_todo: 0
        })
    }
}
