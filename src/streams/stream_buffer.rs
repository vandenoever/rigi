use std::cmp;
use std::convert::From;

pub struct StreamBuffer<T: Clone + Copy + From<u8>> {
    buf: Vec<T>,
    pos: usize,
    avail: usize
}

impl<T: Clone + Copy + From<u8>> StreamBuffer<T> {
    pub fn with_capacity(cap: usize) -> StreamBuffer<T> {
        let mut v = Vec::new();
        v.resize(cap, T::from(0));
        StreamBuffer {
            buf: v,
            pos: 0,
            avail: 0
        }
    }
    fn make_space(&mut self, needed: usize) {
        let mut space = self.buf.len() - self.pos - self.avail;
        if space >= needed {
            // there's enough space
            return;
        }
        // number of items available for reading
        if self.avail > 0 {
            // move data to the start of the buffer
            if self.pos != 0 {
                for i in 0..self.avail {
                    self.buf[i] = self.buf[i + self.pos];
                }
                space += self.pos;
            }
        } else {
            // we can use the whole buffer
            space = self.buf.len();
        }
        self.pos = 0;
        if space >= needed {
            return;
        }
        // still not enough space, we have to allocate more
        // we set the size to the reserved amount so no memory is wasted
        self.buf.reserve(needed - space);
        let cap = self.buf.capacity();
        self.buf.resize(cap, T::from(0));
    }
    pub fn read(&mut self, max: usize) -> (&[T], usize) {
        let len = cmp::min(max, self.avail);
        let end = self.pos + len;
        let buf = &self.buf[self.pos..end];
        self.pos += len;
        self.avail -= len;
        (buf, self.avail)
    }
    // give back a buffer that can be written in, of at least size 1
    pub fn get_write_buffer(&mut self, min: usize) -> &mut [T] {
        let min = if min < 1 { 1 } else { min };
        self.make_space(min);
        let offset = self.pos + self.avail;
        &mut self.buf[offset..]
    }
    pub fn avail(&self) -> usize {
        self.avail
    }
    pub fn rewind(&mut self, n: usize) {
        self.pos -= n;
        self.avail += n;
    }
    pub fn reset(&mut self) {
        self.pos = 0;
        self.avail = 0;
    }
    pub fn report_written(&mut self, n: usize) {
        self.avail += n;
    }
}

