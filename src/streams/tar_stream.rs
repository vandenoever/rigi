use std::io;
use std::iter::Iterator;
use streams::Substream;
use streams::Stream;
use streams::StreamInfo;
use streams::FileType;
use std::collections::hash_map::HashMap;

pub struct TarStreamInfo<'a> {
    file_name: &'a Vec<u8>,
    props: &'a HashMap<Vec<u8>,Vec<u8>>,
    mtime: i64,
    stream: Substream<'a, u8>,
    file_type: FileType
}

impl<'a> TarStreamInfo<'a> {
}

impl<'a> StreamInfo<u8> for TarStreamInfo<'a> {
    fn file_name(&self) -> Option<&[u8]> {
        Some(&self.file_name)
    }
    fn properties(&self) -> &HashMap<Vec<u8>,Vec<u8>> {
        &self.props
    }
    fn mtime(&self) -> Option<i64> {
        Some(self.mtime)
    }
    fn file_type(&self) -> FileType {
        self.file_type
    }
    fn stream(&mut self) -> Option<&mut Stream<u8>> {
        Some(&mut self.stream)
    }
}

struct TarHeader {
    size: u64,
    mtime: i64,
    file_type: FileType
}

enum FileName {
    Long(usize),
    Normal
}

fn file_name_len(h: &[u8]) -> usize {
    h[..100].iter().position(|&v| v == 0).unwrap_or(100)
}

fn check_header(h: &[u8]) -> bool {
    if h.len() < 257 {
        // header is too small to check
        return false;
    }
    // the file starts with a file_name of at most 100 characters. The file_name
    // is ended by a \0, after this \0 only \0 is allowed
    let file_name_len = file_name_len(h);
    if file_name_len == 0 {
        return false;
    }
    if h[file_name_len..100].iter().any(|&v| v != 0) {
        return false;
    }
    // there should be some non-zero bytes
    if !h[100..256].iter().any(|&v| v != 0) {
        return false;
    }
    // check for field values that should be '\0' for the header to be a
    // tar header. Two positions are also accepted if they are ' ' because they
    h[107] == 0 && h[115] == 0 && h[123] == 0 && (h[135] == 0 || h[135] == 20) && (h[147] == 0 || h[147] == 20) && h[256] == 0
}

// we read octal fields for size or mtime which are both 11 bytes followed by
// one zero byte. 11 octal bytes can contain 33 bits of information
// valid octal numbers are 0x30-0x37, so  v & 0xf8 == 0x30
fn read_octal_field(b: &[u8]) -> io::Result<u64> {
    assert!(b.len() >= 12);
    if b[0..11].iter().any(|&v| 0xf8 & v != 0x30) || b[11] != 0 {
        return Err(io::Error::new(io::ErrorKind::InvalidData, "invalid octal"));
    }
    Ok(b[0..11].iter().fold(0, |acc, &v| 8 * acc + (v & 0x07) as u64))
}

fn round_up_to_block_usize(size: usize) -> usize {
    let last_block_use = size & 0x1ff;
    if last_block_use == 0 { size } else { size + 512 - last_block_use }
}
fn round_up_to_block_u64(size: u64) -> u64 {
    let last_block_use = size & 0x1ff;
    if last_block_use == 0 { size } else { size + 512 - last_block_use }
}

fn parse_long_header(stream: &mut Stream<u8>, file_name: &mut Vec<u8>, size: usize) -> io::Result<()> {
    let buf = try!(stream.read_n(round_up_to_block_usize(size)));
    file_name.extend_from_slice(&buf[..size]);
    Ok(())
}

fn parse_header(stream: &mut Stream<u8>) -> io::Result<TarHeader> {
    let hb = try!(stream.read_n(512));
    // check for terminators ('\0') on the first couple of fields
    if !check_header(hb) {
        return Err(io::Error::new(io::ErrorKind::InvalidData,
            "Not a valid tar header."));
    }

    let size = try!(read_octal_field(&hb[124..136]));
    let mtime = try!(read_octal_field(&hb[136..148])) as i64;
    let file_type = match hb[156] {
        0 | 48 => FileType::File,
        53 => FileType::Directory,
        49 => FileType::Symlink,
        _ => FileType::Unknown
    };
    Ok(TarHeader{
        size: size,
        mtime: mtime,
        file_type: file_type
    })
}

// A tar file should end with at least 2 blocks of '\0' bytes
// This function reads the stream until the end if it is valid. If invalid
// blocks are encountered, checking ends earlier.
fn check_end(stream: &mut Stream<u8>) -> io::Result<()> {
    let mut num_zero_blocks = 0;
    loop {
        match stream.read_n(512) {
            Err(e) => {
                if e.kind() == io::ErrorKind::UnexpectedEof
                        && num_zero_blocks > 1 {
                    return Ok(());
                }
                return Err(e);
            },
            Ok(buf) => {
                let all_zero = buf.iter().all(|&v| v == 0);
                if !all_zero {
                    return Err(io::Error::new(io::ErrorKind::InvalidData,
                        "Invalid tail for tar file."));
                }
            }
        }
        num_zero_blocks += 1;
    }
}

fn is_at_end(stream: &mut Stream<u8>) -> io::Result<bool> {
    let at_end = try!(stream.read_n(512)).iter().all(|&v| v == 0);
    stream.rewind(512);
    Ok(at_end)
}

fn parse_file_name(stream: &mut Stream<u8>, file_name: &mut Vec<u8>) -> io::Result<FileName> {
    let hb = try!(stream.read_n(512));
    // check for terminators ('\0') on the first couple of fields
    if !check_header(hb) {
        return Err(io::Error::new(io::ErrorKind::InvalidData,
            "Not a valid tar header."));
    }

    let size = try!(read_octal_field(&hb[124..136]));
    let file_name_start = if hb.starts_with(b"./") { 2 } else { 0 };
    let mut file_name_end = file_name_len(hb);
    if file_name_end != 2 && hb[file_name_end - 1] == '/' as u8 {
        file_name_end -= 1;
    }
    let name = &hb[file_name_start..file_name_end];
    if name == b"./@LongLink" {
        return Ok(FileName::Long(size as usize));
    }
    file_name.extend_from_slice(name);
    Ok(FileName::Normal)
}

fn create_tar_substream<'a>(stream: &'a mut Stream<u8>, file_name: &'a mut Vec<u8>, props: &'a HashMap<Vec<u8>,Vec<u8>>) -> io::Result<(TarStreamInfo<'a>,u64)> {
    file_name.clear();
    match try!(parse_file_name(stream, file_name)) {
        FileName::Normal => {
            if file_name.len() == 0 {
                // skip this entry which is only a header block
                return create_tar_substream(stream, file_name, props);
            }
            // rewind the block so the rest of the header can be parsed
            stream.rewind(512);
        },
        FileName::Long(size) => {
            try!(parse_long_header(stream, file_name, size as usize));
        }
    }
    let header = try!(parse_header(stream));
    let position = stream.position();
    // header has been read and stream is at the start of the file
    Ok((TarStreamInfo {
        file_name: file_name,
        props: props,
        stream: try!(Substream::new(stream, header.size)),
        mtime: header.mtime,
        file_type: header.file_type,
    }, position + round_up_to_block_u64(header.size)))
}

pub struct TarStream<'a> {
    stream: &'a mut Stream<u8>,
    file_name: Vec<u8>,
    props: HashMap<Vec<u8>,Vec<u8>>,
    next_position: u64
}

impl<'a> TarStream<'a> {
    pub fn new(stream: &mut Stream<u8>) -> io::Result<TarStream> {
        if stream.position() != 0 {
            return Err(io::Error::new(io::ErrorKind::InvalidData,
                "Stream is not at the start."));
        }
        if let Some(size) = stream.size() {
            if size % 512 != 0 {
                return Err(io::Error::new(io::ErrorKind::InvalidData,
                    "File size is not a multiple of 512."));
            }
        }
        let valid_header = check_header(try!(stream.read_n(512)));
        stream.rewind(512);
        if !valid_header {
            Err(io::Error::new(io::ErrorKind::InvalidData,
                "Not a valid tar file."))
        } else {
            Ok(TarStream{
                stream: stream,
                file_name: Vec::new(),
                props: HashMap::new(),
                next_position: 0
            })
        }
    }
    pub fn next(&mut self) -> io::Result<Option<TarStreamInfo>> {
        let position = self.stream.position();
        if Some(position) == self.stream.size() {
            return Ok(None);
        }
        if position > self.next_position {
            panic!("Stream has advanced more than should be possible.");
        } else {
            // consume the remainder of the previous stream
            try!(self.stream.skip(self.next_position - position));
        }
        if try!(is_at_end(self.stream)) {
            try!(check_end(self.stream));
            return Ok(None);
        }
        let (sub, next_position) = try!(create_tar_substream(self.stream,
                &mut self.file_name, &self.props));
        self.next_position = next_position;
        return Ok(Some(sub));
    }
}
