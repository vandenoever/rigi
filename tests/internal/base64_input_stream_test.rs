extern crate rigi;

use rigi::streams::Base64InputStream;
use rigi::streams::FileInputStream;
use rigi::streams::Stream;
use shared_test_code::input_stream_tests::*;
use std::path::Path;

fn file(stream: &mut Stream<u8>, test: fn(&mut Stream<u8>)) {
    let mut base64 = Base64InputStream::base64(stream);
    test(&mut base64);
}

#[test]
fn base64() {
    let path = Path::new("tests/data/base64enc.txt");
    test_on_file(path, file);
}

#[test]
fn base64_cmp() {
    let mut f1 = FileInputStream::open("tests/data/base64enc.txt").unwrap();
    let mut s = Base64InputStream::base64(&mut f1);
    let mut f2 = FileInputStream::open("tests/data/base64enc.out").unwrap();
    let b1 = s.read(790).unwrap();
    {
        let b2 = f2.read(790).unwrap();
        assert_eq!(b1, b2);
    }
    let b2 = f2.read_min(0).unwrap();
    let b3 : &[u8] = &[];
    assert_eq!(b3, b2);
}

fn slice(s: &mut Stream<u8>, test: fn(&mut Stream<u8>)) {
    let mut base64 = Base64InputStream::base64(s);
    test(&mut base64);
}

#[test]
fn slice_1() {
    test_on_stream1(b"AAAA", slice);
}
#[test]
fn slice_2() {
    test_on_stream2(b"AAAA", slice);
}
#[test]
fn slice_3() {
    test_on_stream3(b"AAAA", slice);
}
