extern crate rigi;

use rigi::streams::Stream;
use shared_test_code::input_stream_tests::*;

fn slice(s: &mut Stream<u8>, test: fn(&mut Stream<u8>)) {
    test(s);
}

#[test]
fn slice_1() {
    test_on_stream1(b"HI", slice);
}
#[test]
fn slice_2() {
    test_on_stream2(b"HI", slice);
}
#[test]
fn slice_3() {
    test_on_stream3(b"HI", slice);
}
