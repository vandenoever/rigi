extern crate rigi;

use rigi::streams::GzipInputStream;
use rigi::streams::Stream;
use shared_test_code::input_stream_tests::*;
use std::path::Path;

fn file(stream: &mut Stream<u8>, test: fn(&mut Stream<u8>)) {
    let mut gzip = GzipInputStream::gzip(stream, false);
    test(&mut gzip);
}

#[test]
#[ignore]
fn gzip() {
    let path = Path::new("tests/data/a.gz");
    test_on_file(path, file);
}
