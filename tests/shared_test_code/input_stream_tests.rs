use rigi::streams::FileInputStream;
use rigi::streams::SliceStream;
use rigi::streams::Stream;
use std::fmt::Debug;
use std::io;
use std::path::Path;

fn assert_io<A: Eq + Debug>(a: io::Result<A>, b: A) {
    match (a,b) {
        (Ok(av),bv) => assert_eq!(av, bv),
        (Err(e),_) => panic!(e)
    }
}

fn input_stream_test1(s: &mut Stream<u8>) {
    assert_io(s.skip(0), ());
    assert_eq!(s.position(), 0);
    let n = match s.size() {
        None => {
            let mut n = 0;
            loop {
                let r = s.read_min(0);
                println!("{:?}", r);//s.read_min(0));
                let buf = r.unwrap();
                if buf.len() == 0 {
                    break;
                }
                n += buf.len();
            }
            n as u64
        },
        Some(size) => {
            s.read(size as usize).unwrap().len() as u64
        }
    };
    assert_eq!(s.position(), n);
    let size = s.size();
    assert_eq!(Some(s.position()), size);
}

fn input_stream_test2(s: &mut Stream<u8>) {
    let p = s.position();
    let len = s.read_min(1).unwrap().len();
    assert!(len >= 1);
    s.rewind(len);
    assert_eq!(p, s.position());
    input_stream_test1(s);
}

fn input_stream_test3(s: &mut Stream<u8>) {
    // try to read beyond the end, which fails
    // confirm that the position has not moved
    let p = s.position();
    let toread = match s.size() {
        Some(size) => size + 1,
        None => 10000000
    } as usize;
    s.read(toread).err();
    println!("SIZE {} {}", s.size().is_some(), s.position());
    assert_eq!(s.position(), p);
    // now the size must be known
    let len = s.size().unwrap();
    {
        let r = s.read(len as usize);
        let nread = r.unwrap().len() as u64;
        assert_eq!(nread, len);
    }
    assert_eq!(s.position(), len);
    // check that no more can be read
    assert!(s.read_min(1).is_err());
    assert_eq!(s.read_min(0).unwrap().len(), 0);
}
pub type T = fn(&mut Stream<u8>);
pub type F = fn(&mut Stream<u8>, T);
fn run_test_on_file(path: &Path, f: F, t: T) {
    let mut fi = FileInputStream::open(path).unwrap();
    f(&mut fi, t);
}
pub fn test_on_file(path: &Path, f: F) {
    run_test_on_file(path, f, input_stream_test1);
    run_test_on_file(path, f, input_stream_test2);
    run_test_on_file(path, f, input_stream_test3);
}
fn run_test_on_slice(slice: &[u8], f: F, t: T) {
    let mut v = Vec::new();
    v.extend_from_slice(slice);
    let mut s = SliceStream::new(v);
    f(&mut s, t);
}
pub fn test_on_stream1(slice: &[u8], f: F) {
    run_test_on_slice(slice, f, input_stream_test1);
}
pub fn test_on_stream2(slice: &[u8], f: F) {
    run_test_on_slice(slice, f, input_stream_test2);
}
pub fn test_on_stream3(slice: &[u8], f: F) {
    run_test_on_slice(slice, f, input_stream_test3);
}
